//
//  StationsVC.swift
//  Delhi Metro
//
//  Created by Usman Shaukat on 31/10/2020.
//  Copyright © 2020 Usman Shaukat. All rights reserved.
//

import UIKit

class StationsVC: UIViewController {

    @IBOutlet weak var stationsTableView: UITableView!
    
    let stations = [
        "Karol Bagh",
        "Jhande Wala",
        "RK Ashram",
        "Rajiv Chowk",
        "Barakhamba",
        "Mandi House",
        "Patel Chowk",
        "New Delhi",
        "Okhla",
        "INA"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func leftSideMenuButtonTapped(_ sender: Any) {
        let scene = UIApplication.shared.connectedScenes.first
        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
            sd.mySideMenuController?.revealMenu()

        }

    }

}


extension StationsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return stations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StationCell", for: indexPath) as! StationCell
                 
        cell.stationName.text = stations[indexPath.row]
        
        return cell;
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        weak var weakSelf = self
        
        let alert = UIAlertController(title: stations[indexPath.row], message: stations[indexPath.row], preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil ))
        alert.addAction(UIAlertAction(title: "Share", style: .default, handler: { action in
            let items = ["This is the station and fare details"]
            let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
            weakSelf?.present(ac, animated: true)
        } ))

        self.present(alert, animated: true)
        
    }
    
}
