//
//  SideMenuVC.swift
//  Delhi Metro
//
//  Created by Usman Shaukat on 31/10/2020.
//  Copyright © 2020 Usman Shaukat. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController {

    let items = ["Home", "History", "Favorites", "Share with Friends"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func showStationsVC(){
     
        let homeSB = UIStoryboard(name: "Home", bundle: nil)
        let stationsVC = homeSB.instantiateViewController(withIdentifier: "StationsVC") as! StationsVC
        let navVC = UINavigationController(rootViewController: stationsVC)
        
        let scene = UIApplication.shared.connectedScenes.first
        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
            sd.mySideMenuController?.contentViewController = navVC
            sd.mySideMenuController?.contentViewController?.navigationController?.isNavigationBarHidden = false
            sd.mySideMenuController?.hideMenu()
        }

    }
    
    func showShareController(){
        let alert = UIAlertController(title: "Share With Friends", message: "Share this app with your friends on social media", preferredStyle: .actionSheet)

            
        alert.addAction(UIAlertAction(title: "WhatsApp", style: .default, handler: { action in
            let msg = "Delhi Metro"
            let urlWhats = "whatsapp://send?text=\(msg)"
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                if let whatsappURL = NSURL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                        UIApplication.shared.openURL(whatsappURL as URL)
                    } else {
                        // Cannot open whatsapp
                    }
                }
            }

        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil ))

        self.present(alert, animated: true)
    }
    
}

extension SideMenuVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
              
        cell.itemLabel.text = items[indexPath.row]
        
        return cell;
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            showStationsVC()
        }
        else if indexPath.row == 3 {
            showShareController()
        }
    }
}
