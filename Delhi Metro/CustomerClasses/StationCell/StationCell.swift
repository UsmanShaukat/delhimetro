//
//  StationCell.swift
//  Delhi Metro
//
//  Created by Usman Shaukat on 31/10/2020.
//  Copyright © 2020 Usman Shaukat. All rights reserved.
//

import UIKit

class StationCell: UITableViewCell {

    @IBOutlet weak var stationName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
