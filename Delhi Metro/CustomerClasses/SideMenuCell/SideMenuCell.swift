//
//  SideMenuCell.swift
//  Delhi Metro
//
//  Created by Usman Shaukat on 31/10/2020.
//  Copyright © 2020 Usman Shaukat. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var itemIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
