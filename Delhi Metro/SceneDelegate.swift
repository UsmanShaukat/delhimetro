//
//  SceneDelegate.swift
//  Delhi Metro
//
//  Created by Usman Shaukat on 31/10/2020.
//  Copyright © 2020 Usman Shaukat. All rights reserved.
//

import UIKit
import SideMenuSwift

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var navigationController: UINavigationController?
    var mySideMenuController : SideMenuController?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        self.initRootViewController(scene: scene)

    }

    func initRootViewController(scene: UIScene){
        
         let sideMenuSB = UIStoryboard(name: "SideMenu", bundle: nil)
         let sideMenuVC = sideMenuSB.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC

         let homeSB = UIStoryboard(name: "Home", bundle: nil)
         let stationsVC = homeSB.instantiateViewController(withIdentifier: "StationsVC") as! StationsVC


         navigationController = UINavigationController(rootViewController: stationsVC)
         navigationController?.isNavigationBarHidden = false
        
         guard let winScene = (scene as? UIWindowScene) else { return }
         window = UIWindow(windowScene: winScene)
         
         self.mySideMenuController = SideMenuController(
            contentViewController:  navigationController!,
            menuViewController:     sideMenuVC
         )
         window?.rootViewController = mySideMenuController
         window?.makeKeyAndVisible()

        SideMenuController.preferences.basic.menuWidth = 280.0
        
         SideMenuController.preferences.basic.statusBarBehavior = .none
         SideMenuController.preferences.basic.position = .above
         SideMenuController.preferences.basic.direction = .left
         SideMenuController.preferences.basic.enablePanGesture = true
         SideMenuController.preferences.basic.enablePanGesture = true
         SideMenuController.preferences.basic.enableRubberEffectWhenPanning = false
    
    }

    func sceneDidDisconnect(_ scene: UIScene) {
    
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
    
    }

    func sceneWillResignActive(_ scene: UIScene) {
    
    }

    func sceneWillEnterForeground(_ scene: UIScene) {

    }

    func sceneDidEnterBackground(_ scene: UIScene) {

    }


}

